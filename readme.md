# Rosette Paper 

See the [**LATEST BUILD**](/../-/jobs/artifacts/master/raw/rosette_paper_draft.pdf?job=compile_pdf) of the paper.

## Building with Docker 

This repository contains the draft of my paper about motion tracking via rosette navigators as well as a Dockerfile for building a container based on Ubuntu Linux and Texlive. This ensures that you can build the paper from source in an environment identical to the author's without needing to worry about latex installations.

Assuming Docker is installed, the paper can be built by running the following commands:

```bash
./docker_build.sh
./docker_run.sh
```
To obtain Docker, see:

Linux Users: https://www.docker.com/community-edition

Windows/Mac Users: https://www.docker.com/products/docker-toolbox

## Paper Outline

An outline (work in progress) of the paper can be found here:

https://dynalist.io/d/uU0okWJDn0P1G9j8zqTnbLYV