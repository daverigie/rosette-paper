import os, shutil
import glob, re
import subprocess
import platform

output_format = 'png'
vector_extensions = ('pdf','svg','emf')
mogrify_flags = ' '.join(['-alpha', 'remove', '-density', '300', '-format', output_format])

def cmd(cmd_string):
    return subprocess.call(cmd_string, shell=True)

def ishiddendir(dirname):
    return re.match('.*\.\w.*', dirname)

def list_vectors(rootdir):
    """ List all files in subfolders underneath root matching vector_extensions. Skip hidden dirs. """
    filelist = []
    for root, dirs, files in os.walk(rootdir):
        if ishiddendir(root) or (rootdir == root):
            continue
        if os.path.basename(root) in vector_extensions:
            for f in files:
                filelist.append( os.path.join(root, f) )

    return filelist

def do_mogrify(filepath):

    if platform.system() == 'Windows':
        output = do_mogrify_windows(filepath)
    else:
        output = do_mogrify_linux(filepath)
   
    figures_dir = os.path.dirname(os.path.dirname(filepath))
    outputpath  = os.path.splitext(filepath)[0] + "." + output_format
    newpath     = os.path.join(figures_dir, 
                               output_format,  
                               os.path.splitext(os.path.basename(filepath))[0] + "." + output_format)

    shutil.move(outputpath, newpath)
    
def do_mogrify_windows(filepath):
    command_str = "mogrify " + mogrify_flags + " {}".format(filepath)
    cmd(command_str)
    
def do_mogrify_linux(filepath):
    print('Not implemented yet')
    cmd('pwd')
        
if __name__ == "__main__":
    filelist = list_vectors('..')
    for i,file in enumerate(filelist):
        print("Processing file {} of {} [{}]".format(i+1, len(filelist)+1, file))
        do_mogrify(file)

    

