# LaTeX Makefile
SOURCE_DIR := ./src
ROOT_DIR   := ./


.PHONY : pdf

build: debug cleansource

debug: 
	(cd $(SOURCE_DIR); make all)
	(cp $(SOURCE_DIR)/*.pdf $(ROOT_DIR))
	cd $(ROOT_DIR)

clean:
	rm *.pdf
	
cleansource:
	(cd $(SOURCE_DIR); make cleanall)
	
cleanall: cleansource clean