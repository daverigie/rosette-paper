# Notes and Revisions

## From Meeting (1/18/2018)

### To Do

- [x] Talk about SSFP and challenges of detecting cardiac motion at 3T and during PET-MR (me or Fernando?)
- [ ] Add justification for rosette trajectory (Fernando can write this)
- [x] Human Study -> Human Studies
- [x] Get the number of human cases right
- [x] Discuss patient 3 cardiac signal looking different, possible detection of "dicrotic notch"
- [ ] Do an MR scan with pulse oximeter/EKG and belt for comparison

