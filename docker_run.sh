#!/bin/bash
# Once the docker image is built, this will launch a container, compile the .TEX source and create the PDF output
docker run --rm -v $(pwd):/home/rosettepaper/ rosettepaper /bin/bash -c "make clean; make; make clean"