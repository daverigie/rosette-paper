FROM ubuntu:15.04

MAINTAINER David Rigie

RUN apt-get update && \
    apt-get upgrade -y

RUN apt-get install -y texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended texlive-fonts-extra texlive-lang-all

RUN apt-get install -y build-essential
RUN apt-get install -y latexmk

WORKDIR /home/rosettepaper

CMD ["bash"]