\input{rosette_paper_preamble}

\begin{document}
\input{title_page}

%%% ABSTRACT %%%           
\begin{abstract}
\noindent \textbf{Purpose:} 
To develop a flexible method for tracking respiratory and cardiac
motions throughout MR and PET-MR body examinations that requires no additional hardware and minimal 
sequence modification.
\\ 
\textbf{Methods:} 
The incorporation of a contrast-neutral rosette-navigator module
following the RF excitation allows for robust cardiorespiratory motion tracking with
minimal impact on the host sequence. Spatial encoding gradients are applied to the
FID signal and the desired motion signals are extracted with a blind-source separation
technique. This approach is validated with an anthropomorphic, PET-MR-compatible 
motion phantom as well as in several human studies. 
\\
\textbf{Results:} 
Both respiratory and cardiac motions were reliably extracted from
the proposed rosette navigator in phantom and patient studies. In the phantom
study, the MR-derived motion signals were additionally validated against the 
ground-truth measurement of diaphragm displacement and left-ventricle model triggering
pulse.
\\
\textbf{Conclusion:} The proposed method yields accurate respiratory and cardiac
	motion-state tracking, requiring only a short (1.76 ms) additional navigator
	module, which is self-refocusing and imposes minimal constraints on sequence design.
\end{abstract}

%%% SECTION:INTRODUCTION %%%   
\section{Introduction}
Many strategies have been proposed for tracking respiratory and cardiac (a.k.a cardiorespiratory) motions during MRI; however a robust and general solution that is clinically feasible is still elusive. Cardiorespiratory motion tracking is important in a variety of clinical contexts. For example, free-breathing abdominal imaging has emerged as an attractive alternative to breath-hold procedures, due to the higher scan efficiency and improved patient comfort. This technique relies on the acquisition of a one-dimensional respiratory gating signal in conjunction with the imaging data, so that the data can be retrospectively sorted into respiratory motion states prior to image reconstruction. When imaging the heart, both respiratory and cardiac motions need to be managed. Therefore, one either needs to track both respiratory and cardiac motion or employ some combination of cardiac triggering/gating and breath-holds. Another emerging application of cardiorespiratory motion tracking is for motion correction of positron emission tomography (PET) data during a simultaneous PET-MR exam. Following the recent introduction of integrated PET-MR \cite{Delso2011}, several works have demonstrated that motion information obtained from MR data can be used to compensate for patient motion during the PET image reconstruction.

One approach to motion tracking is to use external sensors, such as the respiratory bellows and electrocardiogram (ECG). However, this increases the patient setup time and does not always yield reliable results \cite{Santelli2011}. In particular, the bellows require careful positioning, and optimal placement depends on whether the patient is a chest breather or abdominal breather \cite{Reimer2010}. The signal amplitude may not provide an accurate surrogate for diaphragm position, particularly when the diaphragm position drifts throughout the scan. Similarly, ECG-based cardiac motion tracking also faces several practical challenges. The voltage signal that is measured by ECG may not correlate well with actual heart motion, especially for patients with arrhythmia. Additionally, at high fields, magnetohydrodynamic (MHD) effects distort the ECG signal, which further complicates tracking \cite{Krug2011}.

A potential alternative to external tracking devices is the use of MR navigators, which derive motion information directly from the MR signal. Besides eliminating the need to setup additional devices, navigators also provide a more direct measurement of internal organ motion and hence may yield more reliable motion tracking signals. Pencil beam navigators have been used for tracking diaphragm motion \cite{Ouyang2013} but the additional selective RF pulse can decrease scan efficiency and alter the imaging signal. It has been shown that a variety of pulse sequences are ``self-navigating," such as radial sequences which repeatedly sample a central point (or line) in k-space. In this case, motion information is obtained directly from the same data collected for image formation, so the aforementioned limitations of navigator echoes are avoided. However, in this case, motion information would only be available during particular sequences which have this property, which excludes most clinical sequences. Further, the nature of the motion encoding would vary between different self-gating sequences. 

Brau et al. propose a solution \cite{Brau2006} which essentially converts an arbitrary imaging sequence into a self-navigated one by inserting a short ($20 \mu s$) readout immediately after slice selection and gradient refocusing. Respiratory information is obtained directly from this free induction decay (FID) signal, and only trivial sequence modifications are required; image contrast and scan efficiency are virtually unchanged. Similar FID navigators, utilizing information from multiple coil channels, have also been used to track head motion \cite{Dyverfeldt2014}. In this work, we expand on this concept by adding a small amount of spatial encoding during the readout of the FID and introduce an unsupervised learning method for automatically extracting the respiratory and cardiac motion surrogate signals from the raw navigator data. In particular, we utilize a self-refocused rosette trajectory, which effectively encodes cardiorespiratory motion without impacting image contrast. This approach mostly retains the versatility of a pure FID navigator while improving the robustness of cardiac motion extraction. Therefore, it has the potential to be a robust, general solution for simultaneously tracking respiratory and cardiac motion during arbitrary pulse sequences without the need for external tracking devices. This could be particularly useful for motion correction during simultaneous PET-MR exams. In particular, this manuscript is a significant expansion upon our previous work in Rigie et al \cite{Rigie2017}.

In the first part of this manuscript, we validate the proposed navigation method and automatic motion-parameter extraction with a dynamic thorax phantom \cite{Fieseler2013, Bolwin2018} that provides realistic and controllable cardiorespiratory motion. Subsequently, the technique is applied to several human patient scans in order to demonstrate robustness and clinical feasibility. 

%%% SECTION:METHODS %%%   
\section{Methods}
All scans were performed on a 3T Siemens Biograph mMR (Siemens Healthcare, Erlangen, Germany) using a T1-weighted, golden angle radial stack-of-stars in-house prototype sequence (TR/TE=12.7/5.3ms, BW=488Hz/Pixel, 80x4.5mm slices, Figure 1), modified to incorporate a rosette \cite{Noll1998} navigator readout. Respiratory and cardiac motion surrogates were extracted from the multi-coil navigator data via the second-order blind identification (SOBI) algorithm \cite{Belouchrani1997}, which was subsequently used to sort k-space data into motion states. 

%%% SUBSECTION:SELF-REFOCUSED ROSETTE NAVIGATOR %%%
\subsection{Self-refocused Rosette Navigator}

The proposed navigator is placed directly after the RF pulse, prior to the frequency encoding gradient and has a readout length of 1.76 milliseconds. Because of its short duration and self-refocusing trajectory, it can be incorporated into a variety of clinical sequences without impacting image contrast and while imposing minimal constraints on the sequence design. The trajectory is shown in figure \ref{fig:rosette} along with a diagram of the rosette-navigated radial stack-of-stars sequence used in the this work. 

%%% FIG:ROSETTE %%%
\begin{figure*}[htb]
	\centering
	\includegraphics[width = 5in]{01_rosette_traj_and_pulseseq.pdf}
	\caption{The rosette navigator is placed directly after the RF excitation and prior to the imaging readout. Its short duration (1.76 ms) and self-refocusing trajectory ensure minimal impact on image contrast.}
	\label{fig:rosette}
\end{figure*}


%%% SUBSECTION:RESPIRATORY AND CARDIAC MOTION EXTRACTION %%%
\subsection{Respiratory and Cardiac Motion Extraction}

Because of the short duration of the navigator readout compared to the respiratory and cardiac motions of interest \cite{Davies1994}, we neglect any motion occurring during this time and consider each navigator readout to be a static snapshot of the present motion state. Thus, at intervals of $\Delta t = TR$, we get a complex data vector $\vec{x}$ with length $M = N_{\mathrm{coils}}\cdot N_{\mathrm{samp}}$ given by the product of the number of coil elements and the number of points sampled along the trajectory. We refer to this vector hereafter as a ``coil fingerprint". The total number of coil fingerprints is given by $N = \tau/TR$, where $\tau$ is the total scan time. Typically, each coil fingerprint may be on the order of $10^3-10^5$ elements depending on the number of coil channels. Prior to motion extraction, the coil fingerprints need to be arranged into a 2D real matrix. This is accomplished by first concatenating each $M$-element navigator readout as separate columns into an $M\times N$ complex matrix $X$. This complex matrix is then expanded into to the real-valued, $(2M)\times N$ ``coil-fingerprint matrix" (CFM) $X'$, where
%
\begin{equation}
X' = \begin{bmatrix}
\Re(X) \\ \Im(X)
\end{bmatrix}.
\end{equation}

The extraction of motion surrogates from the CFM involves three primary steps: (1) dimension reduction, (2) blind source separation, and (3) motion component identification. 

\paragraph{Dimension Reduction.}
Dimension reduction is achieved via principal component analysis (PCA). The matrix $X'$ is reduced to only a handful of components which characterize most of the variation in the coil fingerprints. The reduced representation is given by
%
\begin{equation}
T_L = W_LX'
\end{equation}
%
where the rows of $W_L$ are the $L$ largest eigenvectors of $XX^T$, and $L$ is chosen so that $\lambda_\mathrm{min}/\mathrm{Tr}(XX^T) \geq 0.005$. Hence, the discarded eigenvectors account for no more than 0.5\% of the total variance. %
In some cases, the respiratory and cardiac surrogates can already be clearly identified within the components of $T_L$. However, often these signals are fused to one another or contaminated by artifacts.

\paragraph{Blind Source Separation.}
The previous PCA step already yields components (the rows of $T_L$) which are uncorrelated because PCA diagonalizes the covariance matrix. However, this condition by itself is insufficient for identifying independent signals. To see why that is, consider $T_L$ with zero mean and unit variance. The rows being uncorrelated implies $T_L T_L^H = I$; however we can form another signal matrix $T'_L = T_LQ$, where $Q$ is an arbitrary orthogonal matrix, and by construction $T'_L$ also has uncorrelated rows because $T'_L (T'_L)^H = T_L Q Q^H T_L = T_L T_L^H = 1$. Therefore, the principal components may actually represent mixtures of various motion signals and sequence dependent artifacts unrelated to motion. 

SOBI breaks this indeterminacy by considering a set of autocovariance matrices at different time lags $\tau_1, \tau_2, \ldots \tau_p$. The $\tau$-time-lagged covariance matrix $C(\tau)$ for a given signal matrix $X$ is given by
%
\begin{equation}
C_{ij}(\tau) = x_i(t-\tau)x^H_j(t).
\label{eq:timelaggedcovariance}
\end{equation}
%
Thus, SOBI seeks to jointly diagonalize $C(\tau_1), C(\tau_2), \ldots C(\tau_p)$. Since it is generally not possible to diagonalize more than two matrices simultaneously, SOBI instead finds an approximate solution my minimizing the following joint diagonalization (JD) criterion:
%
\begin{equation}
V^* = \argmin_V \quad \sum_{k=1}^p \mathrm{off}\left(V^HC(\tau_k)V \right).
\label{eq:jd}
\end{equation}
%
The ``off" of an $n\times n$ matrix $M$ is defined as
%
\begin{equation}
\label{eq:off}
\mathrm{off}(M) \equiv \sum_{i \neq j} |M_{ij}|^2
\end{equation}
%
If a single time lag $\tau=0$ were used, the approach would be equivalent to principal component analysis; however, by considering multiple time lags, the robustness of the source identification is greatly enhanced \cite{Belouchrani1997}. The result is not very sensitive to the number of time lags used, but larger $p$ generally leads to improved robustness at the cost of longer computation time. There is an efficient procedure for minimizing [\ref{eq:jd}], which is described fully in \cite{Belouchrani1997}.

\paragraph{Motion Component Identification.}

Finally, the respiratory and cardiac signals are automatically identified by selecting the SOBI component $I(t)$ that maximizes the ratio
%
\begin{equation}\label{eq:spectraldensity}
r = \frac{\int_{f_\mathrm{min}}^{f_\mathrm{max}} \left|\hat{I}(f)\right|^2\,df}%/
{\int_{-\infty}^{\infty} \left|\hat{I}(f)\right|^2\,df},
\end{equation}
%
where $\hat{I}(f)$ is the Fourier Transform of $I(t)$, and the frequency window $\left[f_\mathrm{min}, \, f_\mathrm{max}\right]$ is selected to encompass a reasonable range for either respiration or heart rate. For all subjects, we have used $[0.1 \Hz, 0.6 \Hz]$ and $[0.8 \Hz, 1.7 \Hz]$ for respiratory and cardiac, respectively. This criterion favors signals with a fundamental frequency inside of the expected physiological range and also disfavors signals with spurious peaks outside of this range. We find that this helps to reject periodic artifacts even if their fundamental frequency is similar to the physiological signals because they have prominent harmonics at higher frequencies, whereas the quasi-periodic physiological motions do not.

These steps of dimension reduction, blind source separation, and motion component selection are all linear operations, so they can be concatenated into a single linear operation. As a result, two weight vectors $w_r$ and $w_c$ are learned, which relate the respiratory and cardiac motion surrogates $\phi_r$ and $\phi_c$ back to the coil fingerprints:
%
\begin{align}
\phi_r &= w_r^HX'\\
\phi_c &= w_c^HX' .
\end{align}
% 
Thus, once this motion model training is performed, subsequent navigators could be converted to motion gating signals on-the-fly. We found 1 minute of training data to be adequate. After this training phase, subsequent navigators could be used for real-time triggering.

%%% SUBSECTION:DATA ORGANIZATION AND IMAGE RECONSTRUCTION %%%
\subsection{Data Organization and Image Reconstruction}
Once motion surrogates are extracted from the rosette fingerprints, they can be used to reconstruct dynamic image frames. First, the 1D respiratory and cardiac surrogate signals are obtained. Subsequently, they are used to partition the MR data into a discrete number of motion states based on the amplitude of the respiratory and cardiac surrogates, $I_R(t)$ and $I_C(t)$. For simplicity we set the thresholds for each partition so that all motion states have approximately the same amount of MR data. In this work, we used 5 respiratory and 5 cardiac states, resulting in 25 total motion states. Any number of motion states could be used; however, there exists a trade-off between image quality and motion blurring. Practically, the number of motion states used in the reconstruction is a design choice impacted by the desired reconstruction speed, available memory on the reconstruction server, and image acquisition time. Our primary interest is in PET-MR motion correction, where typically between 5 and 10 motion states are used for both respiratory and cardiac binning. See, for example \cite{Ouyang2013, Kustner2017}.

Our MRI reconstruction model is very similar to that of \cite{Feng2016} and minimizes the objective function
%
\begin{equation}
\argmin_d \quad \frac{1}{2}\|FCd-m\|_2^2 + \lambda_r \|\nabla_r d\|_1 + \lambda_c\|\nabla_c d\|_1 + \lambda_{xyz}\|\nabla_{xyz} d\|_1,
\label{eq:costfun}
\end{equation}
%
where $F$ is the nonuniform fast Fourier transform (NUFFT) and $\nabla_c$ and $\nabla_r$ are finite differencing operators along the cardiac and respiratory dimensions, respectively. $C$ and $m$ can be expanded as 
%
\begin{align}
	C = \begin{bmatrix}
		C_1 \\ C_2 \\ \vdots \\ C_{\mathrm{N_{coils}}}\end{bmatrix}, \quad m = \begin{bmatrix} m_1 \\ m_2 \\ \vdots \\ m_{\mathrm{N_{coils}}}\end{bmatrix},
\end{align}
%
where $C_i$ and $m_i$ are the coil sensitivity map and k-space data from the $i^\mathrm{th}$ coil element, respectively. The optimizer $d$ is a 5D image volume with three spatial dimensions and two extra motion dimensions indexing the respiratory and cardiac states. Additional details on the data sorting can also be found in \cite{Feng2016}. To solve [\ref{eq:costfun}], we performed 30 iterations of the nonlinear conjugate gradient algorithm with corner rounding to handle the non-differentiable total variation penalties and set $\lambda_c = \lambda_r = \lambda_{xyz} = 0.01$. 

%%% SUBSECTION:PHANTOM STUDY %%%
\subsection{Phantom Study}

Evaluation of respiratory and cardiac motion extraction was performed with an MR-compatible, anthropomorphic motion phantom \cite{Bolwin2018}. The phantom (see figure 2) is a life-sized human torso model with a plastic thorax, inflatable silicone lungs, a deformable left ventricle model (LVM), and a liver compartment. Respiration is simulated by driving the diaphragm with a pneumatic piston, while cardiac motion is achieved by inflating the inner cavity of the LVM with water. Contraction occurs passively due to the elasticity of the LVM. The position of the diaphragm and cardiac triggering are recorded to a log file, which provides a ground truth against which we validated our navigator-based motion extraction. Specifically, we compared the respiratory motion surrogate to the analog measurement of the diaphragm displacement and the cardiac motion surrogate to the LVM triggering pulses. 

\begin{figure}
\centering
\includegraphics[width=\linewidth]{wilhelm-diagram-and-photo}
\caption{Photograph of the motion phantom (left) and a diagram (right) showing its compartments. Respiratory motion is achieved by driving the diaphragm structure with a pneumatic piston. Cardiac motion is achieved by inflating the inner cavity of the left ventricle model with water.}
\label{fig:wilhelm-diagram}
\end{figure}

The MR imaging was performed on the Siemens Biograph mMR according to the specifications mentioned earlier with 1280 radial views, resulting in a scan time of 10 minutes. This scan was repeated five times under different motion conditions:

\begin{itemize}
\item Static End Inspiration - The LVM and diaphragm are fixed with the diaphragm in its most inferior position.
\item Static End Expiration - The LVM and diaphragm are fixed with the diaphragm in its most superior position.
\item Respiration Only (Periodic) - The LVM is fixed and respiration occurs with a constant period.
\item Respiration Only (Aperiodic) - The LVM is fixed and respiration occurs with quasi-random variations in breathing rate and depth.
\item Respiration and Cardiac Motion (Aperiodic) - Both cardiac and respiratory motions are simulated. The respiration model matches the previous condition.
\end{itemize}

Respiratory and cardiac motion surrogates were automatically extracted and used to reconstruct dynamic MR image frames according to the procedure outlined previously.

%%% SUBSECTION:HUMAN STUDY %%%
\subsection{Human Studies}

In addition to the phantom study, we also tested the proposed motion extraction procedure on $\numPatients$  human subjects. MRI scanning parameters are identical to the phantom study except only 800 radial views were acquired, resulting in a scan time of approximately 6 minutes. Motion extraction, MR data sorting, and reconstruction were performed in an identical manner to the phantom study. In this case, the motion surrogates were visually confirmed by ensuring that the reconstructed image frames contain respiratory and cardiac motions when traversing the corresponding motion dimension. Additionally, all but one of the human subjects were also scanned with an alternate version of the sequence wherein the gradients were not applied during the navigator readout. In this case, the navigator is a pure FID navigator. This serves as a control to demonstrate the importance of the frequency encoding information obtained due to the rosette trajectory. 

%%% SUBSECTION:EXTENSION TO 2D SEQUENCES %%%
\subsection{Extension to 2D Sequences}
Several additional human studies were performed as a proof-of-concept to illustrate how the proposed techniques can be incorporated into 2D multi-slice imaging sequences. This is more challenging than the 3D case since the excited volume changes throughout imaging. The rosette navigator module was embedded in a 2D, T2-weighted turbo spin-echo (TSE) sequence (TR=1s, BW=521 Hz/pixel, 32x4 mm slices, 1.09 mm in-plane resolution). An extra non-selective RF pulse was also added directly before the navigator readout. This was not necessary in the 3D case because, during imaging, the entire volume is excited, so the imaging signal can be used both for imaging and navigation. A small flip-angle of six degrees is used, so that the navigation has minimal impact on the imaging signal. 

Separating the motion signals from sequence related artifacts is also slightly more challenging in the 2D case because the spurious signals from other slices contaminate the navigator data. The temporal behavior of these sequence-related artifacts is constant for all scans, so they can be estimated and subtracted by performing a reference scan of a static phantom. Subsequent data processing is identical to the 3D acquisition. This navigated TSE sequence was repeated on six human volunteers.

%%% SECTION:RESULTS %%%
\section{Results}

\subsection{Phantom Study}

Figure \ref{fig:wilhelm_sobi_components} shows the 13 components obtained after performing dimension reduction (PCA) and blind source separation (SOBI) on the rosette coil fingerprint data from the phantom study with both respiratory and cardiac motions enabled. Prior to dimension reduction, this navigator data had a dimensionality of $45056$. The respiratory and cardiac components, which were automatically identified according to [\ref{eq:spectraldensity}], are color-coded in red and blue, respectively. The extracted respiratory and cardiac signals are overlayed on top of the reference measurements (black) provided by the phantom in Figure \ref{fig:analog}. Both of the signals extracted from the navigator data match up very well with the reference signals.

%%% FIG:ICA %%%
\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.85\linewidth]{wilhelm_sobi_components}
	\caption{Shown above are the 13 components extracted by the SOBI algorithm. The respiratory (red) and cardiac components (blue) are automatically selected according to the criterion described in equation \ref{eq:spectraldensity}.}
	\label{fig:wilhelm_sobi_components}
\end{figure}

%%% FIG:REFERENCE SIGNAL %%%
\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.9\linewidth]{wilhelm-allcomponents-extraction-card-resp-ground-truth}
	\caption{The respiratory (red) and cardiac (blue) signals obtained from the navigator are compared to ground truth measurements from the phantom. For respiration the reference signal is an analog measurement of diaphragm position, and for cardiac motion, the reference signal is the triggering pulse.}
	\label{fig:analog}
\end{figure}

Figure \ref{fig:fusedpetmr} shows a sagittal slice of the fused PET and MR reconstructions of the motion phantom, arranged into a $5\times 5$ motion-state matrix. Each row contains 5 distinct respiratory states at a fixed cardiac state, while each column contains 5 distinct cardiac states at a fixed respiratory state. The contraction of the heart structure is visually apparent when comparing the top and bottom rows. The respiratory motion is more difficult to discern in this static representation but some superior/inferior diaphragm and heart displacements can be observed, moving from left to right along a row of the motion-state matrix. 
%
%%% FIG:WILHELM PET-MR %%%
\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.9\linewidth]{%
		04_wilhelm_petmr_motion_matrix%
	}
	\caption{%
		The respiratory and cardiac signals are used to sort the MR and PET listmode data. In this case, the respiratory and cardiac cycles were each divided into five unique states, resulting in 25 total motion states. The associated MR and PET reconstructions are shown in the fused images above (sagittal plane). Each column comprises five distinct cardiac states at a fixed respiratory state. Likewise each row comprises five distinct respiratory states at a fixed cardiac state.%
	}
	\label{fig:fusedpetmr}
\end{figure}

The two static scans, taken at end inspiration and end expiration, were used to validate the motion states reconstructed from the dynamic data, which is depicted in figure \ref{fig:wilhelmdiff}. Panels (a) and (d) were reconstructed from the static end expiration and end inspiration scans, respectively. Panels (b) and (e) are the corresponding motion state images reconstructed from the dynamic data based on the respiratory and cardiac gating signals in figure \ref{fig:analog}. Panels (c) and (f) are the difference images for end expiration and end inspiration respectively. In general, agreement is good but some structures are slightly blurred in the dynamic reconstructions, such as the diaphragm.

%%% FIG:WILHELM DIFF IMAGES %%%
\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.9\linewidth]{%
		wilhelm_difference_images%
	}
	\caption{%
		Above, we compare ground truth images acquired of the static phantom fixed in either end expiration (a) or end inspiration (d) to the corresponding motion states reconstructed from the gated motion-enabled: (b) is end expiration and (e) is end inspiration. In the static scans, the heart is fixed at end systole. Images (c) and (f) are the difference images for end expiration and end inspiration respectively.%
	}
	\label{fig:wilhelmdiff}
\end{figure}

%%% SUBSECTION: HUMAN STUDY %%%

\subsection{Human Studies}

Figure \ref{fig:humanrespcard} shows the respiratory and cardiac motion surrogates that were extracted from the navigator data for the four human subjects. Each of these signals appears to be a plausible descriptor of respiratory/cardiac motion in terms of shape and frequency. The gating signals were verified qualitatively by reconstructing dynamic MR image sequences and observing the resulting videos. The cardiac gating signal for Patient 3 appears to exhibit a ``dicrotic notch," which is discussed further in the Discussion section.

%
%%% FIG: HUMAN RESP/CARD GATING %%%
\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.75\linewidth]{%
		Patient-gating-signal-summary%
	}
	\caption{%
		The low-pass-filtered respiratory and cardiac signals obtained from the navigator are shown above for four human patients. These signals are extracted according the steps described above in a fully automated manner. The cardiac gating signal for Patient 3 appears to exhibit a ``dicrotic notch," which is discussed further in the Discussion section.%
	}
	\label{fig:humanrespcard}
\end{figure}
%
As in Figure \ref{fig:fusedpetmr} all 25 motion state images (sagittal plane) from one patient are shown in Figure \ref{fig:humanmotionmatrix}.
%
%%% FIG:HUMAN DUAL GATED RECON %%%
\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.9\linewidth]{%
		06_patient1_motion_matrix%
	}
	\caption{%
		The respiratory and cardiac signals are used to sort the MR data into 25 distinct motion states. In this case, the respiratory and cardiac cycles were each divided into five states. The associated MR reconstructions are shown in the images above (sagittal plane). Each column comprises five distinct cardiac states at a fixed respiratory state. Likewise each row comprises five distinct respiratory states at a fixed cardiac state.%
	}
	\label{fig:humanmotionmatrix}
\end{figure}
%
As a control, we also acquired several datasets with the navigator gradients switched off. We still have a 1.76$\mu$s readout following the RF pulse; since there is no spatial encoding, it is just a pure FID signal. It is well known that respiratory motion can be derived from repetitive data acquired at the k-space center. However, we were unable to reliably extract cardiac motion from only the FID navigator data. Figure \ref{fig:navonnavoff} illustrates the SOBI components obtained from our motion extraction procedure for the pure FID navigator (left) and the rosette navigator (right). The rosette spatial encoding improves the accuracy of the respiratory motion estimate (red) and makes it possible to extract cardiac motion (blue). 
%
%%% FIG: HUMAN RESP/CARD GATING %%%
\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.9\linewidth]{%
	patient15_gating_signals_navOff_navOn_combined%
	}
	\caption{%
		Shown above is a comparison between the rosette navigator (right) and the control (left), in which no spatial encoding gradients are played out resulting in a pure FID navigator. The spatial encoding improves the accuracy of the respiratory motion estimate (red) and also makes it possible to extract cardiac motion (blue) %
	}
	\label{fig:navonnavoff}
\end{figure}

We also extracted respiratory and cardiac gating signals from a modified TSE sequence, described earlier. This was repeated in six volunteers. A representative example is shown in figure \ref{fig:tse}. 

%%% FIG: TSE Gating Signals %%%
\begin{figure}[htb]
	\centering
	\includegraphics[width = 0.9\linewidth]{%
	tse_resp_card_signals_after_cca_filtering%
	}
	\caption{%
		Shown here are the respiratory (red) and cardiac (blue) gating signals obtained from the rosette navigator data embedded in a 2D, T2-weighted turbo spin-echo sequence. In this case, a non-selective pulse is required prior to the navigator readout because the imaging is done slice-by-slice.
	}
	\label{fig:tse}
\end{figure}

%%% SECTION DISCUSSION %%%
\section{Discussion}

The proposed cardiorespiratory motion tracking technique, using self-refocusing rosette navigators, has been experimentally validated with an MR-compatible motion phantom. The respiratory and cardiac motion surrogates derived from the navigator data matched up well with the ground truth diaphragm displacement and cardiac triggering signals. The gating signals were further validated by performing gated 5D reconstructions with the cardiac and respiratory motion states representing two additional motion dimensions. Additionally, the method has been successfully applied to $\numPatients$ human volunteer datasets, demonstrating its robustness in a realistic clinical setting. 

Our findings indicate that a pure FID navigator is insufficient for extracting cardiac motion and that the additional spatial encoding provided, e.g. by the proposed rosette navigator is necessary for dual gating. Some other works have reported success in extracting cardiac motion signals from a k-space-center navigator \cite{Feng2016, Liu2010}, but these utilized balanced steady-state free precession (SSFP) sequences, which have excellent contrast between the blood pool and the myocardium \cite{Thiele2001}. Imaging with SSFP sequences is much more challenging at 3T due to shorter $T_2^*$ values and larger $B_0$ inhomogeneities \cite{Schar2004}, which cause off-resonance artifacts. For this reason, cardiac imaging at 3T more commonly utilizes fast gradient echo sequences, which have very poor contrast in the heart, making it more challenging to extract a cardiac gating signal. We also note that, so far, all commercially available PET-MR scanners have 3T magnets. Therefore, our proposed navigation technique may be of particular interesting for cardiac motion-correction in PET-MR.

One interesting observation is that there exists a prominent notch in cardiac gating signal extracted from Patient 3, which is not seen in the other examples. A similar feature is sometimes observed in pulse oximetry and referred to as a “dicrotic notch.” The dicrotic notch coincides with a small dip in aortic pressure caused by the closing of the aortic valve during ventricular systole. While this is a possible explanation for the shape of this cardiac gating signal, comparison with a pulse oximeter reading would be needed for confirmation.

In these studies, the full process of extracting motion surrogates from the navigator data was completely automated using the same settings for all datasets. They key algorithmic design choices were determining the number of principal components to retain during the dimensionality reduction step and the frequency windows to use for identifying the respiratory and cardiac motion components. For the dimensionality reduction step, we found it effective to choose the cutoff based on the size of the eigenvalues $\lambda_1$, $\lambda_2$, $\ldots$ $\lambda_N$ relative to $\lambda_\mathrm{max}$. The specific cutoff used in this study of 0.5\% was chosen empirically. This choice is mostly important for recovering the cardiac motion surrogate, since the respiratory motion tends to be mostly concentrated in the first principal component, while the cardiac motion signal is relatively weaker. The respiratory and cardiac frequency windows of $[0.1 \Hz, 0.6 \Hz]$ and $[0.8 \Hz, 1.7 \Hz]$ were selected to cover wide range of typical respiration and heart rates. In practice, these windows could be more narrowly customized by measuring the patient's heart and respiration rate prior to the examination. 


While the proposed method offers a number of advantages to other approaches, such as not needing external tracking devices and being compatible with a variety of pulse sequences, it does have several practical limitations. Firstly, it requires modifying all MRI pulse sequences for which motion tracking is desired, whereas external tracking devices operate independent of the imaging sequence. While the navigator readout is relatively short (1.76 ms) it may still impose some design limitations on rapid T1-weighted imaging sequences with very short repetition times.

%%% SECTION CONCLUSIONS %%%
\section{Conclusions}

We have demonstrated a robust, flexible method for tracking respiratory and cardiac motion automatically using rosette navigators. The proposed navigation method can be incorporated into a variety of sequences without impacting image contrast. The approach has been validated on an anthropomorphic motion phantom, which provides ground truth motion measurements, as well as $\numPatients$ human datasets. The extracted motion information has been used to perform motion-gated reconstructions from scans acquired during free breathing. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                    REFERENCES           
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibliography{rosettenav}


\end{document}
